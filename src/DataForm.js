import React, { useState } from 'react';
import './DataForm.css';

function DataForm() {
  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [person, setPerson] = useState({});
  const [error, setError] = useState('');

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleAgeChange = (event) => {
    setAge(event.target.value);
  };

  const validateInput = () => {
    let isValid = true;
    setError('');

    if (!name.match(/^[A-Z]/)) {
      setError('Name must start with a capital letter.');
      isValid = false;
    }

    if (!age.match(/^\d+$/) || parseInt(age, 10) < 0 || parseInt(age, 10) > 125) {
      setError('Age must be an integer between 0 and 125.');
      isValid = false;
    }

    return isValid;
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (validateInput()) {
      setPerson({
        name: name,
        age: parseInt(age, 10)
      });
    }
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <label>
          Name:
          <input type="text" value={name} onChange={handleNameChange} />
        </label>
        <label>
          Age:
          <input type="text" value={age} onChange={handleAgeChange} />
        </label>
        <button type="submit">Submit</button>
        {error && <p className="error-message">{error}</p>}
      </form>
      <p>{JSON.stringify(person)}</p>
    </div>
  );
}

export default DataForm;
